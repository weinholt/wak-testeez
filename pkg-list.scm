; pkg-list.scm - define dorodango packaging metadata
; Copyright 2011 David Banks <amoebae@gmail.com>; license LGPL 2.1 or later.

(package (wak-testeez (0 2))
  (depends (spells)
           (wak-common))
  
  (synopsis "Lightweight Unit Test Mechanism")
  (description 
   "Testeez is a simple lightweight unit test mechanism for R5RS Scheme. It was"
   "written to support regression test suites embedded within the source code"
   "files of the author's portable Scheme libraries.")
  (libraries
   (sls -> "wak")
   ("testeez" -> ("wak" "testeez"))))
