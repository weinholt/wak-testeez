# wak-testeez.mk - build dorodango packages from this distribution
# Copyright 2011 David Banks <amoebae@gmail.com>; license LGPL 2.1 or later.

package_name = wak-testeez

# Basic smoke test for create-bundle
test:
	@tmp_d=$$(mktemp -dt $(package_name)-XXXXXXXX); \
	tmp_f=$$(mktemp -t $(package_name)-XXXXXXXX.zip); \
	rsync -rt --exclude-from exclude.rsf ./ $$tmp_d; \
	doro create-bundle -o $$tmp_f $$tmp_d; \
	unzip -l $$tmp_f; \
	rm -rf $$tmp_d $$tmp_f

build:
	@tmp_d=$$(mktemp -dt $(package_name)-XXXXXXXX); \
	tmp_f=$$(mktemp -t $(package_name)-XXXXXXXX.zip); \
	rsync -rt --exclude-from exclude.rsf ./ $$tmp_d; \
	doro create-bundle -o $$tmp_f $$tmp_d
